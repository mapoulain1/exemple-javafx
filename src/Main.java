import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.util.Random;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");

        StackPane root = new StackPane();
        //setupNuke(root);
        setupBorderPane(root, primaryStage);


        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }

    private void setupBorderPane(Pane pane, Stage primaryStage) {
        Button[] buttons = new Button[5];
        for (int i = 0; i < 5; i++) {
            buttons[i] = new Button("Zone " + i);
            buttons[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            buttons[i].setOnAction(new EventHandler<ActionEvent>() {
                                       @Override
                                       public void handle(ActionEvent event) {
                                            //primaryStage.setFullScreen(!primaryStage.isFullScreen());
                                            //primaryStage.setWidth(primaryStage.getWidth()/2);
                                           Thread t = new Thread(){
                                               double r = 0;
                                               @Override
                                                public void run() {
                                                    super.run();
                                                    while (true){
                                                        primaryStage.setWidth(primaryStage.getWidth() + 0.1);
                                                        primaryStage.setHeight(primaryStage.getHeight() + 0.1);
                                                        for (int j = 0; j < 5; j++) {
                                                            buttons[j].setRotate(r);
                                                            r+=0.1;
                                                            try {
                                                                Thread.sleep(10);
                                                            } catch (InterruptedException ignored) {

                                                            }
                                                        }

                                                    }


                                                }
                                            };
                                           t.start();


                                       }
                                   }
            );
        }


        BorderPane borderPane = new BorderPane(buttons[0], buttons[1], buttons[2], buttons[3], buttons[4]);
        pane.getChildren().add(borderPane);
    }


    private void setupNuke(Pane pane) {
        Button btn = new Button();
        btn.setText("NUKE BOMB");


        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                for (int i = 0; i < 10; i++) {

                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setContentText("Catch me if you can !");


                    alert.show();
                    Thread t = new Thread() {
                        Random random = new Random();
                        int x = random.nextInt(1366 - 360);
                        int y = random.nextInt(768 - 180);
                        double speedX = random.nextDouble() * 3 + 1.5;
                        double speedY = random.nextDouble() * 3 + 1.5;
                        double dirX = random.nextBoolean() ? -speedX : speedX;
                        double dirY = random.nextBoolean() ? -speedY : speedY;
                        int iteration = 0;
                        int iterationLimit = random.nextInt(100);

                        @Override
                        public void run() {
                            super.run();
                            while (true) {
                                alert.setX(x);
                                alert.setY(y);
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException ignored) {

                                }
                                x += dirX;
                                y += dirY;
                                if (x > 1366 - 360)
                                    dirX = -speedX;
                                if (x < 0)
                                    dirX = speedX;

                                if (y > 768 - 180)
                                    dirY = -speedY;
                                if (y < 0)
                                    dirY = speedY;

                                if (iteration > iterationLimit) {
                                    iteration = 0;
                                    speedX = random.nextDouble() * 3 + 1.5;
                                    speedY = random.nextDouble() * 3 + 1.5;
                                    iterationLimit = random.nextInt(100);
                                }
                                iteration++;
                            }
                        }
                    };
                    t.start();
                }


            }
        });
        pane.getChildren().add(btn);
    }

}